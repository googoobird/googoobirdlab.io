export default class LastMessage {
  constructor(userMessageData){

    this.toTitle = '';
    this.messages  = [];
    this.fromTitle = '';
    if(userMessageData?.lang === 'langEn'){
      this.toTitle = `To. ${userMessageData.name}`;
      this.messages = [
        'We’ll always treasure',
        'the moments shared with you.',
        'Thank you for being a part of',
        'our journey, KDG!'
      ];
      this.fromTitle = 'SE SO NEON';
    }else{
      this.toTitle = `${userMessageData.name}에게`;
      this.messages = [
        '네가 곁에 있는 지금을 늘 떠올릴게.',
        '함께해줘서 고마워 나의 코딱지!'
      ];
      this.fromTitle = '새소년';
    }
  }
}