import IndexView from "./view/Index.js";
import Step1View from "./view/Step1.js";
import Step2View from "./view/Step2.js";
import Step3View from "./view/Step3.js";

export default class Router{


  hash = {
    index : '#index',
    step1 : '#step1',
    step2 : '#step2',
    step3 : '#step3'
  }
  view = {
    index : null,
    step1 : null,
    step2 : null,
    step3 : null,
    step4 : null
  };
  
  constructor(){
    this.runInit = false;
    this.init();
  }

  init(){
    window.addEventListener('popstate', () => {
      this.setRoute();
    });
    this.setRoute();
  }

  getHash(){
    return location.hash;
  }

  setDescription(msg){
    const headerDiv = document.querySelector('div#header');
    headerDiv.innerHTML = `<h3>${this.getHash()} : ${msg}</h3>`;
  }

  setRoute(){
    if(this.runInit){
      switch(this.getHash()){
        case this.hash.step1:
      
          if(!this.view['step1']){
            this.view.step1 = new Step1View();
          }
          this.view.step1.render();
        break;
  
        case this.hash.step2:
          
          if(!this.view['step2']){
            this.view.step2 = new Step2View();
          }
          this.view.step2.render();
        break;
  
        case this.hash.step3:
          if(!this.view['step3']){
            this.view.step3 = new Step3View();
          }
          this.view.step3.render();
        break;
  
        case this.hash.index:
        default:
          if(!this.view['index']){
            this.view.index = new IndexView();
          }
          this.view.index.render();
        break;
      }
    }else{
      this.runInit = true;
      history.pushState(null,null, '#index');
      window.dispatchEvent(new Event('popstate'));
    }
  }
}