import LastMessage from "../data/LastMessage.js";
import APIClient from "../net/ApiClient.js";
export default class Step3View{
  
  constructor(){
    this.init();
  }

  init(){
    this.WAIT_TIME = 30 * 1000;
    this.WAIT_TIMER = null;
    this.inputEventHandler = (evt)=>{
      const targetId = evt.target.id;
      switch(targetId){
        case 'arrowLeft':
          evt.target.style.display = 'none';
          this.moveToFirstPage();
        break;
      }
    }
  }

  render(){

    this.saveExcel();

    if(this.WAIT_TIMER){
      clearTimeout(this.WAIT_TIMER);
      this.WAIT_TIMER = null;
    }

    this.registerHelper();
    const handlebarsTpl = document.querySelector(`#step3Tpl`).innerHTML;
    const handlerTpl = Handlebars.compile(handlebarsTpl);
    const contentDiv = document.querySelector('div#content');

    const lastMessage = new LastMessage(USER_MESSAGE_DATA);
    contentDiv.innerHTML = handlerTpl(lastMessage);

    const bgWrap = contentDiv.querySelector('div#gifDisplayWrap');
    this.getGifBg(bgWrap);
    this.runHandAnimation(()=>{
      this.runLastMessageAnimation();
    }, false);
  }

  registerHelper(){
    Handlebars.registerHelper('setLangClass', function (context,option) {
      let langClass = 'ko-font';
      if(USER_MESSAGE_DATA.lang === LANG_EN){
        langClass = 'en-font';
      }
      return  new Handlebars.SafeString(`class="${langClass}"`);
    });
  }

  getGifBg(targetElement){
    targetElement.innerHTML = '<div id="openHand"></div>';
  }

  runHandAnimation(animationCallBack, isRevers){
    const openHandElem = document.querySelector('div#openHand');
    const lastIndex = (isRevers)? 0 : 9;
    let loopIndex = (isRevers)? 8 : 1;
    let runAnimationInterval =  setInterval(()=>{
      let runCondition = (isRevers)? loopIndex > lastIndex : loopIndex < lastIndex;
      if(runCondition){
        openHandElem.style.backgroundPosition = `-${loopIndex*100}vw`;
      }else{
        try{
          clearInterval(runAnimationInterval);
        }catch(excp){
          console.error(excp);
        }
        runAnimationInterval = null;
        if(animationCallBack){
          animationCallBack();
        }
      }
      loopIndex = (isRevers)? loopIndex - 1 : loopIndex + 1;
    },180);
  }

  moveToFirstPage(){
    const arrowLeftButton = document.querySelector('div#arrowLeft');
    if(arrowLeftButton){
      arrowLeftButton.style.display = 'none';
    }
    try{
      USER_MESSAGE_DATA = null;
      if(this.WAIT_TIMER){
        clearTimeout(this.WAIT_TIMER);
        this.WAIT_TIMER = null;
      }
      const _this = this;
      this.runReversMeessage();
      this.runHandAnimation(()=>{
        _this.removeEvents();
        history.pushState(null,null, '#step1');
        window.dispatchEvent(new Event('popstate'));
      },true);
    }catch(excp){
      history.pushState(null,null, '#step1');
      window.dispatchEvent(new Event('popstate'));
    }
  }

  runLastMessageAnimation(){
    this.addEvents();
    const messageWrap = document.querySelector('div#endingMessageWrap');
    const elemWidth = messageWrap.offsetWidth;
    const windowWidth = window.innerWidth;
    const perWOfWin = Math.round((elemWidth/windowWidth) * 100);
    messageWrap.style.marginLeft = `${Math.round((100 - perWOfWin) /2)}%`;

    const elemHeight = messageWrap.offsetHeight;
    const windowHeight= window.innerHeight;
    const perHOfWin = Math.round((elemHeight/windowHeight) * 100);
    messageWrap.style.top = `${Math.round((100 - perHOfWin) /2)}%`;

    messageWrap.style.animation = 'fadein 2s';
    messageWrap.style.animationFillMode = 'forwards';
  }

  runReversMeessage(){
    const messageWrap = document.querySelector('div#endingMessageWrap');
    messageWrap.style.animation = 'fadeout 2s';
  }

  addEvents(){
    const arrowLeftButton = document.querySelector('div#arrowLeft');
    arrowLeftButton.addEventListener('click', this.inputEventHandler);

    const endMessageWrap = document.querySelector('div#endingMessageWrap');
    endMessageWrap.addEventListener('animationend', (evt)=>{
      arrowLeftButton.style.display = 'block';
      this.WAIT_TIMER = setTimeout(()=>{
        this.moveToFirstPage();
      },this.WAIT_TIME);
    },{once : true});
  }

  removeEvents(){
    const arrowLeftButton = document.querySelector('div#arrowLeft');
    arrowLeftButton.removeEventListener('click', this.inputEventHandler);
  }

  saveExcel(){
    const savePath = API_URL + API_PATH.CSV.SAVE;
    const reqOption = {
      method : 'POST',
      headers : {'Content-Type': 'application/json'},
      body : JSON.stringify(USER_MESSAGE_DATA)
    }
    APIClient.fetch(savePath, reqOption).then(data=>{ 
      console.log(data);
    }).catch(error=>{
      console.log(error);
    });
  }
}