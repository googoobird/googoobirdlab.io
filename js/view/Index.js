export default class IndexView{
  constructor(){
    this.init();
  }

  init(){
    //window.addEventListener("contextmenu", e => {console.log('block'); e.preventDefault()});
  }

  setFullScreen(evt){
    console.error("🚀 ~ file: Index.js:11 ~ IndexView ~ setFullScreen ~ evt:", evt)
    if(document.documentElement.requestFullscreen){
      document.documentElement.requestFullscreen();
    }else if(document.documentElement.mozRequestFullScreen){
      document.documentElement.mozRequestFullScreen();
    }else if(document.documentElement.webkitRequestFullscreen){
      document.documentElement.webkitRequestFullscreen();
    } else if(document.documentElement.msRequestFullscreen){
      document.documentElement.msRequestFullscreen();
    }
  
    if(evt.target){
      evt.target.remove();
      setTimeout(() => {
        this.gotoNextPage();  
      }, (800));
    }
  }

  gotoNextPage(){
    history.pushState(null,null, '#step1');
    window.dispatchEvent(new Event('popstate'));
  }

  render(){
    const handlebarsTpl = document.querySelector(`#indexTpl`).innerHTML;
    const handlerTpl = Handlebars.compile(handlebarsTpl);
    const contentDiv = document.querySelector('div#content');
    contentDiv.innerHTML = handlerTpl(null);
    const htmlBody = document.querySelector('div#openButton');

    let _tihs = this;
    htmlBody.addEventListener('click',function clickEventHandler(evt){
      _tihs.setFullScreen(evt);
      htmlBody.removeEventListener('click', clickEventHandler);
    });
  }
}